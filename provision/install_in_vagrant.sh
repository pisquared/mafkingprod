#!/usr/bin/env bash
cd /var/www/mafkingprod && ./INSTALL.sh
cat >> /home/vagrant/.bashrc << EOF
export PS1="\[\e[0;32m\]\u@\h\[\e[m\] \[\e[1;34m\]\w \[\e[m\]\[\e[1;32m\]\$\[\e[m\] "
cd /var/www/mafkingprod
alias a='source venv/bin/activate'
a
EOF