#!/usr/bin/env bash

HELP_TRANSLATION="
USAGE ./manage.sh translate [extract|gen {lang}|compile|update]

    extract             Extract strings in files as defined in translations/babel.cfg
    gen {lang}          Init translations for {lang}
    compile             Compile all translations
    update              Use after a new extract - it may mark strings as fuzzy.
"

command_translate() {
    TRANSLATE_COMMAND=$1
    shift
    case "$TRANSLATE_COMMAND" in
        extract) pybabel extract -F translations/babel.cfg -o translations/messages.pot .
        ;;
        gen) pybabel init -i translations/messages.pot -d translations -l "$@"
        ;;
        compile) pybabel compile -d translations
        ;;
        update) pybabel update -i translations/messages.pot -d translations
        ;;
        *)  >&2 echo -e "${HELP_TRANSLATION}"
        ;;
    esac
    return $?
}

command_translate $@