#!/bin/bash
source venv/bin/activate
export FLASK_WEBAPP_ENV="prod"
export FLASK_APP="webapp.py"
gunicorn -w 4 -b 127.0.0.1:4000 webapp:app
