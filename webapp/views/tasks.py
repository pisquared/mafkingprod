from flask import flash, redirect, request, url_for, render_template
from flask_login import current_user, login_required

from webapp.models.tasks import Task
from webapp.store import db
from webapp.utils import get_now_user
from webapp.views import client_bp


@client_bp.route('/tasks/<int:task_id>/done', methods=['POST'])
@login_required
def done_task(task_id):
    task = Task.query. \
        filter_by(user=current_user, id=task_id).first()
    if not task:
        flash("Task {} doesn't exist".format(task_id), category="error")
        return redirect(request.referrer)
    task.is_done = not task.is_done
    if task.finished_ts:
        task.finished_ts = None
    else:
        task.finished_ts = get_now_user(current_user)
    db.session.add(task)
    db.session.commit()
    flash("Task updated.")
    return redirect(request.referrer)


@client_bp.route('/tasks/<int:task_id>/edit', methods=['GET', 'POST'])
@login_required
def edit_task(task_id):
    task = Task.query.filter_by(id=task_id, user=current_user).first()
    if not task:
        flash("Task doesn't exist.", category="error")
        return redirect(request.referrer)
    if request.method == 'GET':
        return render_template('tasks/task-edit.html', task=task)
    title = request.form.get('title')
    task.title = title
    db.session.add(task)
    db.session.commit()
    flash("Task edited.")
    return redirect(url_for('client_bp.tasks_get'))
