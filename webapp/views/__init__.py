from flask import Blueprint

client_bp = Blueprint('client_bp', __name__)
api_bp = Blueprint('api_bp', __name__, url_prefix="/api")

from webapp.views import main, inbox_entries, tasks, tracker_sessions
