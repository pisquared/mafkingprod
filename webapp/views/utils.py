import calendar
from datetime import datetime, timedelta

from flask import request, flash, redirect, url_for, render_template, jsonify
from flask_login import current_user, login_required
from inflector import English

from webapp import strings, camel_case_to_train_case
from webapp.csrf_protect import csrf
from webapp.model_controller import Runnable
from webapp.models.system import Settings, Log, Message
from webapp.store import db
from webapp.utils import camel_case_to_snake_case, get_today_user, get_tomorrow_user


class Context(object):
    def __init__(self, model_enabled_field=None):
        self.now = None
        self.month_calendar = None
        self.weeknum = None
        self.beginning_of_date = None
        self.end_of_date = None
        self.user_settings = None
        self.model_enabled_field = model_enabled_field

    def buld_calendar(self):
        prev_month = self.now.replace(day=1) - timedelta(days=1)
        next_month = self.now.replace(day=28) + timedelta(days=7)
        cal_inst = calendar.Calendar()
        month_days = [(a, self.now.month, self.now.year, 'today' if a == self.now.day else 'cur') for a in
                      cal_inst.itermonthdays(self.now.year, self.now.month)]
        next_month_days = [(a, next_month.month, next_month.year, 'next') for a in
                           cal_inst.itermonthdays(next_month.year, next_month.month)]
        prev_month_days = [(a, prev_month.month, prev_month.year, 'prev') for a in
                           cal_inst.itermonthdays(prev_month.year, prev_month.month)]
        month_days[:7] = [a if a[0] != 0 else prev_month_days[-7 + i] for i, a in enumerate(month_days[:7])]
        month_days[-7:] = [a if a[0] != 0 else next_month_days[i] for i, a in enumerate(month_days[-7:])]
        today_day_index = month_days.index((self.now.day, self.now.month, self.now.year, 'today'))
        self.month_calendar = [month_days[i:i + 7] for i in range(0, len(month_days), 7)]
        self.weeknum = today_day_index / 7

    def process_date_filter_params(self, default_start=None, default_end=None):
        date_filter = request.args.get('date_filter')
        date_range_filter = request.args.get('date_range_filter')
        datetime_range_filter = request.args.get('datetime_range_filter')

        if date_filter:
            try:
                parsed_date = datetime.strptime(date_filter, "%Y-%m-%d")
            except ValueError:
                raise Exception("Incorrect date format.")
            beginning_of_date = parsed_date.replace(hour=0, minute=0, second=0)
            end_of_date = (parsed_date + timedelta(days=1)).replace(hour=0, minute=0, second=0)
        elif date_range_filter:
            try:
                start_date, end_date = date_range_filter.split('--')
            except ValueError:
                raise Exception("Incorrect date range format.")
            try:
                parsed_start_date = datetime.strptime(start_date, "%Y-%m-%d")
                parsed_end_date = datetime.strptime(end_date, "%Y-%m-%d")
            except ValueError:
                raise Exception("Incorrect date format.")
            beginning_of_date = parsed_start_date.replace(hour=0, minute=0, second=0)
            end_of_date = (parsed_end_date + timedelta(days=1)).replace(hour=0, minute=0, second=0)
        elif datetime_range_filter:
            try:
                start_date, end_date = datetime_range_filter.split('--')
            except ValueError:
                raise Exception("Incorrect date range format.")
            try:
                beginning_of_date = datetime.strptime(start_date, "%Y-%m-%dT%H:%M:%S")
                end_of_date = datetime.strptime(end_date, "%Y-%m-%dT%H:%M:%S")
            except ValueError:
                raise Exception("Incorrect date format.")
        else:
            # get today by default
            beginning_of_date = default_start or get_today_user(current_user)
            end_of_date = default_end or get_tomorrow_user(current_user)
        if beginning_of_date > end_of_date:
            raise Exception("Date range start date is after end date.")

        return beginning_of_date, end_of_date

    def get_or_create_user_settings(self):
        user_settings = Settings.query.filter_by(user=current_user).first()
        if not user_settings:
            user_settings = Settings.create(user=current_user)
            message = Message.create(user=current_user,
                                     body=strings.HELP_MESSAGE_INBOX_ENTRY,
                                     url_context=url_for('client_bp.inbox-entries_get'),
                                     )
            db.session.add(message)
            db.session.add(user_settings)
            db.session.commit()
        return user_settings

    def get_messages(self):
        return Message.query.filter_by(user=current_user, url_context=request.path, dismissed=False).all()

    def build_context(self, model):
        self.user_settings = self.get_or_create_user_settings()
        self.messages = self.get_messages()
        try:
            if self.model_enabled_field:
                model_enabled = getattr(self.user_settings, self.model_enabled_field)
            else:
                model_enabled = getattr(self.user_settings, "{}_enabled".format(model))
        except:
            raise AccessDenied("Access denied motherfucker. Ain't no setting called {}".format(model))
        if not model_enabled:
            raise AccessDenied("Access denied motherfucker. Go to your settings and ask politely for {}".format(model))
        selected_date = request.args.get('selected_date')
        if selected_date:
            try:
                self.now = datetime.strptime(selected_date, '%Y-%m-%d')
            except ValueError:
                raise Exception("Incorrect date format")
        else:
            self.now = datetime.utcnow()
        self.buld_calendar()
        self.beginning_of_date, self.end_of_date = self.process_date_filter_params()
        return self


class AccessDenied(Exception):
    pass


class ViewController(object):
    def __init__(self, model, association_model=None, model_enabled_field=None):
        self.model = model
        self.model_name = model.__name__
        self.model_name_s = camel_case_to_train_case(model.__name__)
        self.model_name_p = English().pluralize(self.model_name_s)
        self.model_name_s_u = camel_case_to_snake_case(model.__name__)
        self.model_name_p_u = English().pluralize(self.model_name_s_u)

        self.model_enabled_field = model_enabled_field

        if association_model:
            self.association_model = association_model
            self.association_model_name_s_u = camel_case_to_snake_case(association_model.__name__)
            self.association_model_name_p_u = English().pluralize(self.association_model_name_s_u)
            self.model_association_model_name_p_u = "{}_{}".format(self.association_model_name_p_u,
                                                                   self.model_name_p_u,
                                                                   )

    def get_template_ctx(self, instances, ctx):
        return dict(instances=instances)

    def process_create_args(self):
        return dict()

    def _get_view_func(self):
        @login_required
        def get_instances():
            try:
                if hasattr(self, 'model_association_model_name_p_u'):
                    ctx = Context(model_enabled_field=self.model_enabled_field).build_context(
                        self.model_association_model_name_p_u)
                else:
                    ctx = Context(model_enabled_field=self.model_enabled_field).build_context(self.model_name_p_u)
            except Exception as e:
                flash(str(e), category="error")
                return render_template('server_error.html')

            filters = [self.model.user == current_user]
            if hasattr(self, 'archived'):
                filters += [self.model.archived == False]
            if issubclass(self.model, Runnable) and ctx.beginning_of_date and ctx.end_of_date:
                filters += [self.model.get_filter_in_bounds(ctx.beginning_of_date, ctx.end_of_date)]
            instances = self.model.query.filter(*filters).all()
            template_ctx = self.get_template_ctx(instances, ctx)
            return render_template("{}/{}.html".format(self.model_name_p, self.model_name_p),
                                   ctx=ctx,
                                   **template_ctx)

        return get_instances

    def _create_view_func(self):
        @login_required
        def create_instance():
            try:
                instance_create_args = self.process_create_args()
            except Exception as e:
                flash(str(e), category="error")
                return redirect(request.referrer)
            instance = self.model.create(log_class=Log,
                                         user=current_user,
                                         **instance_create_args)
            db.session.add(instance)
            db.session.commit()
            flash("{} created.".format(self.model_name))
            return redirect(request.referrer)

        return create_instance

    def _get_instance_by_id(self, instance_id):
        instance = self.model.query.filter_by(id=instance_id, user=current_user).first()
        if not instance:
            raise ("{} doesn't exist".format(self.model_name))
        return instance

    def _archive_instance_view_func(self):
        @login_required
        def archive_instance(instance_id):
            try:
                instance = self._get_instance_by_id(instance_id)
                now = datetime.utcnow()
                instance.archived = True
                instance.archived_ts = now
                db.session.add(instance)
                log = Log.create(user=current_user,
                                 created_ts=now,
                                 body="[{}|{}|archived]".format(self.model_name, instance.id),
                                 )
                db.session.add(log)
                db.session.commit()
            except Exception as e:
                flash(e, category="error")
                return redirect(request.referrer)
            flash("{} archived.".format(self.model_name))
            return redirect(request.referrer)

        return archive_instance

    def _unarchive_instance_view_func(self):
        @login_required
        def unarchive_instance(instance_id):
            try:
                instance = self._get_instance_by_id(instance_id)
                now = datetime.utcnow()
                instance.archived = False
                instance.archived_ts = None
                db.session.add(instance)
                log = Log.create(user=current_user,
                                 created_ts=now,
                                 body="[{}|{}|unarchived]".format(self.model_name, instance.id),
                                 )
                db.session.add(log)
                db.session.commit()
            except Exception as e:
                flash(e, category="error")
                return redirect(request.referrer)
            flash("{} unarchived.".format(self.model_name))
            return redirect(request.referrer)

        return unarchive_instance

    def _delete_instance_view_func(self):
        @login_required
        def delete_instance(instance_id):
            try:
                instance = self._get_instance_by_id(instance_id)
                db.session.delete(instance)
                now = datetime.utcnow()
                log = Log.create(user=current_user,
                                 created_ts=now,
                                 body="[{}|{}|deleted]".format(self.model_name, instance.id),
                                 )
                db.session.add(log)
                db.session.commit()
            except Exception as e:
                flash(e, category="error")
                return redirect(request.referrer)
            flash("{} deleted.".format(self.model_name))
            return redirect(request.referrer)

        return delete_instance

    def register_views(self, bp):
        url = "/{}".format(self.model_name_p)
        bp.add_url_rule(url, endpoint='{name}_get'.format(name=self.model_name_p),
                        view_func=self._get_view_func())

        url = "/{}/create".format(self.model_name_p)
        bp.add_url_rule(url, endpoint='{name}_create'.format(name=self.model_name_p),
                        view_func=self._create_view_func(), methods=['POST'])

        url = "/{}/<int:instance_id>/delete".format(self.model_name_p)
        bp.add_url_rule(url, endpoint='{name}_delete'.format(name=self.model_name_p),
                        view_func=self._delete_instance_view_func())

        url = "/{}/<int:instance_id>/archive".format(self.model_name_p)
        bp.add_url_rule(url, endpoint='{name}_archive'.format(name=self.model_name_p),
                        view_func=self._archive_instance_view_func())

        url = "/{}/<int:instance_id>/unarchive".format(self.model_name_p)
        bp.add_url_rule(url, endpoint='{name}_unarchive'.format(name=self.model_name_p),
                        view_func=self._unarchive_instance_view_func())


class ApiController(ViewController):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def process_batch_create_args(self):
        return [], {}

    def process_batch_create_arg(self, batch_instance, common_params):
        return dict()

    def _create_batch_view_func(self):
        @csrf.exempt
        def create_batch_instances():
            try:
                batch_instances, common_params = self.process_batch_create_args()
            except Exception as e:
                return jsonify({"error": str(e)})
            rv = []
            for batch_instance in batch_instances:
                try:
                    instance_create_args = self.process_batch_create_arg(batch_instance, common_params)
                except Exception as e:
                    return jsonify({"error": str(e)})
                instance = self.model.create(log_class=Log,
                                             user=current_user,
                                             **instance_create_args)
                rv.append(instance.serialize_flat())
            return jsonify({'instances': rv})

        return create_batch_instances

    def register_views(self, bp):
        url = "/{}/create/batch".format(self.model_name_p)
        bp.add_url_rule(url, endpoint='api_{name}_create_batch'.format(name=self.model_name_p),
                        view_func=self._create_batch_view_func(), methods=['GET', 'POST'])


def parse_form_ts(ts_name, all_day=False, parse_from=None):
    if not parse_from:
        parse_from = request.form
    ts_date = parse_from.get('{}_date'.format(ts_name))
    if all_day:
        ts_time = '23:59'
    else:
        ts_time = parse_from.get('{}_time'.format(ts_name))
    try:
        dt = datetime.strptime('{}T{}'.format(ts_date, ts_time), '%Y-%m-%dT%H:%M')
        if all_day:
            dt = dt.replace(second=59, microsecond=999999)
        return dt
    except Exception as e:
        raise Exception("Error parsing {}. Details: {}".format(ts_name, e))
