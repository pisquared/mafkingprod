from flask import flash, redirect, request, url_for, render_template
from flask_login import current_user, login_required

from webapp.models.tasks import Task
from webapp.store import db
from webapp.views import api_bp


@api_bp.route('/tasks/<int:task_id>/done', methods=['POST'])
def done_task(task_id):
    task = Task.query. \
        filter_by(user=current_user, id=task_id).first()
    if not task:
        flash("Task {} doesn't exist".format(task_id), category="error")
        return redirect(request.referrer)
    task.is_done = not task.is_done
    db.session.add(task)
    db.session.commit()
    flash("Task updated.")
    return redirect(request.referrer)
