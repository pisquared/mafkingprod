from datetime import datetime

from flask import flash, redirect, request, render_template, url_for
from flask_login import current_user, login_required

from webapp.models.inbox import InboxEntry
from webapp.models.tasks import Task
from webapp.store import db
from webapp.views import client_bp


@client_bp.route('/inbox-entries/<inbox_entry_id>/edit', methods=['GET', 'POST'])
@login_required
def edit_inbox_entry(inbox_entry_id):
    inbox_entry = InboxEntry.query.filter_by(id=inbox_entry_id, user=current_user).first()
    if not inbox_entry:
        flash("Inbox Entry doesn't exist.", category="error")
        return redirect(request.referrer)
    if request.method == 'GET':
        return render_template('inbox-entries/inbox-entry-edit.html', inbox_entry=inbox_entry)
    body = request.form.get('body')
    inbox_entry.body = body
    db.session.add(inbox_entry)
    db.session.commit()
    flash("Inbox Entry edited.")
    return redirect(url_for('client_bp.get_inbox_entries'))


@client_bp.route('/inbox-entries/<inbox_entry_id>/transform/task')
@login_required
def trasform_inbox_entry_task(inbox_entry_id):
    inbox_entry = InboxEntry.query.filter_by(id=inbox_entry_id, user=current_user).first()
    if not inbox_entry:
        flash("Inbox Entry doesn't exist.", category="error")
        return redirect(request.referrer)

    now = datetime.utcnow()
    task = Task(title=inbox_entry.body, user=current_user, created_ts=now)
    db.session.add(task)
    db.session.commit()

    inbox_entry.is_processed = True
    inbox_entry.processed_type = "task"
    inbox_entry.processed_instance_id = task.id
    db.session.add(inbox_entry)
    db.session.commit()

    flash("Inbox Entry transformed to Task.")
    return redirect(request.referrer)


@client_bp.route('/inbox-entries/<inbox_entry_id>/process')
@login_required
def process_inbox_entry(inbox_entry_id):
    inbox_entry = InboxEntry.query.filter_by(id=inbox_entry_id, user=current_user).first()
    if not inbox_entry:
        flash("Inbox Entry doesn't exist.", category="error")
        return redirect(request.referrer)
    inbox_entry.is_processed = not inbox_entry.is_processed
    db.session.add(inbox_entry)
    db.session.commit()
    flash("Inbox Entry processed.")
    return redirect(request.referrer)
