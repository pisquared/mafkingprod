from flask import redirect, request, url_for, render_template, flash
from flask_login import current_user, login_required

from webapp.models.system import Message, Settings
from webapp.models.tasks import Task
from webapp.store import db
from webapp.views import client_bp
from webapp.views.utils import Context


@client_bp.route('/')
def home():
    if current_user.is_authenticated:
        settings = Context().get_or_create_user_settings()
        if settings.inbox_entries_enabled:
            return redirect(url_for('client_bp.inbox-entries_get'))
        elif settings.tasks_enabled:
            return redirect(url_for('client_bp.tasks_get'))
        else:
            return redirect(url_for('client_bp.get_settings'))
    return redirect(url_for('security.register'))


@client_bp.route('/archive')
@login_required
def get_archive():
    tasks = Task.query.filter_by(user=current_user, archived=True).all()
    return render_template("archive.html",
                           tasks=tasks)


@client_bp.route('/settings', methods=['GET', 'POST'])
@login_required
def get_settings():
    settings = Context().get_or_create_user_settings()
    if request.method == 'POST':
        payload = request.form
        settings_dir = settings.get_bool_fields()
        settings_kv = {k:bool(payload.get(k, False)) for k in settings_dir}
        for k, v in settings_kv.items():
            setattr(settings, k, v)
        db.session.add(settings)
        db.session.commit()
        flash("Settings updated.")
    return render_template("settings.html",
                           settings=settings)


@client_bp.route('/messages/<int:message_id>/dismiss')
@login_required
def dismiss_message(message_id):
    message = Message.query.filter_by(id=message_id, user=current_user).first()
    if not message:
        flash("Message doesn't exist")
        return redirect(request.referrer)
    message.dismissed = True
    db.session.add(message)
    db.session.commit()
    return redirect(request.referrer)


@client_bp.route('/search')
@login_required
def get_search_results():
    q = request.args.get('q')

    tasks = Task.query. \
        whooshee_search(q). \
        filter_by(user=current_user).all()

    return render_template('search-results.html',
                           tasks=tasks,
                           q=q)
