import json
from collections import OrderedDict, defaultdict
from datetime import timedelta, datetime

from flask import request
from flask_security import current_user, login_user

from webapp.models import InboxEntry, Task, CalendarEvent, Note, JournalEntry, Project, Label
from webapp.models.trackers import Tracker, Metric, TrackerSession
from webapp.store import db
from webapp.utils import to_user_tz, generate_random_string, get_now_user, parse_ts, format_seconds_to_human
from webapp.views.utils import ViewController, Context, parse_form_ts, ApiController


class InboxEntryViewController(ViewController):
    def __init__(self, *args, **kwargs):
        super().__init__(InboxEntry)

    def get_template_ctx(self, instances, ctx):
        dated_inbox_entries = OrderedDict()
        for inbox_entry in instances:
            dated_inbox_entries.setdefault(to_user_tz(inbox_entry.created_ts, current_user).date(), []).append(
                inbox_entry)

        not_done_tasks, done_tasks = [], []
        if ctx.user_settings.tasks_enabled:
            tasks = Task.query. \
                filter_by(user=current_user,
                          archived=False). \
                order_by(Task.created_ts.desc()).all()
            not_done_tasks = set([t for t in tasks if not t.is_done])
            done_tasks = set([t for t in tasks if t.is_done])

        inbox_entry_value = request.args.get('inbox_entry_value')
        if inbox_entry_value not in ['task:', ]:
            inbox_entry_value = None
        return dict(
            inbox_entry_value=inbox_entry_value,
            dated_inbox_entries=dated_inbox_entries,
            not_done_tasks=not_done_tasks,
            done_tasks=done_tasks,
        )

    def process_create_args(self):
        body = request.form.get('body', '')
        body = body.strip()
        if not body:
            raise Exception("Empty body of inbox entry.")
        is_processed, processed_type, processed_instance_id = self._attempt_inbox_entry_process(user=current_user,
                                                                                                body=body)
        return dict(
            body=body,
            is_processed=is_processed,
            processed_type=processed_type,
            processed_instance_id=processed_instance_id,
        )

    def _attempt_inbox_entry_process(self, user, body):
        lined_body = body.split('\n')
        settings = Context().get_or_create_user_settings()
        if len(lined_body) > 0:
            first_line_body = lined_body[0]
            first_line_body_lowered = first_line_body.lower().strip()

            if settings.tasks_enabled:
                splittable_words = ['todo:', 'task:']
                for splittable_word in splittable_words:
                    if first_line_body_lowered.startswith(splittable_word):
                        instance_title = splittable_word.join(
                            first_line_body_lowered.split(splittable_word)[1:]).strip()
                        task = Task.create(
                            title=instance_title,
                            user=user)
                        db.session.add(task)
                        db.session.commit()
                        return True, "task", task.id

        return False, None, None


class TasksViewController(ViewController):
    def __init__(self, *args, **kwargs):
        super().__init__(Task)

    def get_template_ctx(self, instances, ctx):
        return dict(
            projects=[(p.id, p.title) for p in Project.get_owned()],
            not_done_tasks=[t for t in instances if not t.is_done],
            done_tasks=[t for t in instances if t.is_done],
        )

    def process_create_args(self):
        title = request.form.get('title', '')
        title = title.strip()
        if not title:
            raise Exception("Empty title of task.")
        return dict(
            title=title
        )


class CalendarEventViewController(ViewController):
    def __init__(self, *args, **kwargs):
        super().__init__(CalendarEvent)

    def get_template_ctx(self, instances, ctx):
        calendar_events = CalendarEvent.get_events_in_bounds(user=current_user,
                                                             start=ctx.beginning_of_date,
                                                             end=ctx.end_of_date)
        calendar_events = sorted(calendar_events, key=lambda x: x.to_start_ts)
        dated_calendar_events = defaultdict(list)
        for calendar_event in calendar_events:
            dated_calendar_events[calendar_event.to_start_ts.date()].append(calendar_event)

        default_start_event = datetime.utcnow().replace(microsecond=0, second=0, minute=0) + timedelta(hours=1)
        default_end_event = default_start_event + timedelta(hours=1)

        default_end_recurring = datetime.utcnow() + timedelta(days=30)

        recurring_types = CalendarEvent.RECURRING_TYPES
        return dict(
            dated_calendar_events=dated_calendar_events,
            default_end_event=default_end_event,
            default_start_event=default_start_event,
            default_end_recurring=default_end_recurring,
            recurring_types=recurring_types,
        )

    def process_create_args(self):
        rv = dict()
        title = request.form.get('title', '')
        title = title.strip()
        if not title:
            raise Exception("Empty title of calendar event.")
        rv['title'] = title

        all_day = bool(request.form.get('all_day', False))
        rv['all_day'] = all_day

        to_start_ts = parse_form_ts('to_start_ts', all_day=all_day)
        rv['to_start_ts'] = to_start_ts
        to_finish_ts = parse_form_ts('to_finish_ts', all_day=all_day)
        if to_finish_ts < rv['to_start_ts']:
            raise Exception("End time can't be before start time.")
        rv['to_finish_ts'] = to_finish_ts

        recurring_type = request.form.get('recurring_type')
        if not recurring_type == CalendarEvent.RECURRING_NONE:
            # processing recurring event
            rv['recurring_to_start_ts'] = to_start_ts
            rv['recurring_to_finish_ts'] = to_finish_ts

            recurring_every = request.form.get('recurring_every')

            rv['recurring_type'] = recurring_type
            rv['recurring_every'] = recurring_every

            if recurring_type == CalendarEvent.RECURRING_WEEKLY:
                weekly_mask = ''.join([str(int(bool(request.form.get('weekly_{}'.format(day), False))))
                                       for day in range(1, 8)])
                rv['recurring_weekly_mask'] = weekly_mask

            recurring_ends_type = request.form.get('recurring_ends_type')
            rv['recurring_ends_type'] = recurring_ends_type

            if recurring_ends_type == CalendarEvent.RECURRING_ENDS_AFTER_N:
                recurring_ends_after_x = request.form.get('recurring_ends_after_x')
                rv['recurring_ends_after_x'] = recurring_ends_after_x
            elif recurring_ends_type == CalendarEvent.RECURRING_ENDS_ON:
                recurring_ends_on = request.form.get('recurring_ends_on')
                if recurring_ends_on < to_start_ts:
                    raise Exception("Recurring can't end before before start time.")
                rv['recurring_ends_on'] = recurring_ends_on
        return rv


class ProjectsViewController(ViewController):
    def __init__(self, *args, **kwargs):
        super().__init__(Project, Task)

    def process_create_args(self):
        title = request.form.get('title', '')
        title = title.strip()
        if not title:
            raise Exception("Empty title of project.")
        return dict(
            title=title
        )


class LabelsViewController(ViewController):
    def __init__(self, *args, **kwargs):
        super().__init__(Label, Task)


class ContextsViewController(ViewController):
    def __init__(self, *args, **kwargs):
        super().__init__(Context, Task)


class NotesViewController(ViewController):
    def __init__(self, *args, **kwargs):
        super().__init__(Note)


class JournalEntryViewController(ViewController):
    def __init__(self, *args, **kwargs):
        super().__init__(JournalEntry)


class TrackerViewController(ViewController):
    def __init__(self, *args, **kwargs):
        super().__init__(Tracker)

    def get_template_ctx(self, instances, ctx):
        metric_types = Tracker.METRIC_TYPES
        return dict(
            trackers=instances,
            metric_types=metric_types,
        )

    def process_create_args(self):
        title = request.form.get('title', '')
        title = title.strip()
        if not title:
            raise Exception("Empty title of project.")
        thing_k = request.form.get('thing_k', '')
        thing_k = thing_k.strip()
        if not thing_k:
            raise Exception("Empty tracker thing_k.")
        api_key = generate_random_string(10)
        rv = dict(
            title=title,
            thing_k=thing_k,
            api_key=api_key,
        )
        for i in range(1, 4):
            metric_k = request.form.get('metric_{}_k'.format(i), '')
            if metric_k:
                metric_t = request.form.get('metric_{}_t'.format(i), '')
                rv["metric_{}_k".format(i)] = metric_k
                rv["metric_{}_t".format(i)] = metric_t
        return rv


class TrackerSessionViewController(ViewController):
    def __init__(self, *args, **kwargs):
        super().__init__(TrackerSession, model_enabled_field="trackers_enabled")

    def process_create_args(self):
        title = request.form.get('title', '')
        title = title.strip()
        if not title:
            raise Exception("Empty tracker session title.")
        rv = dict(
            title=title,
            tracker_id=request.form.get('tracker_id', 0)
        )
        started_ts = parse_form_ts('started_ts')
        rv['started_ts'] = started_ts
        finished_ts = parse_form_ts('finished_ts')
        if started_ts > finished_ts:
            raise Exception("End time can't be before start time.")
        rv['finished_ts'] = finished_ts
        for i in range(1, 4):
            metric_v = request.form.get('metric_{}_v'.format(i), '')
            if metric_v:
                rv["metric_{}_v".format(i)] = metric_v
        return rv

    def get_template_ctx(self, instances, ctx):
        trackers = Tracker.get_owned()
        now = get_now_user(current_user)
        time_ago = now - timedelta(hours=1)
        return dict(
            tracker_sessions=instances[::-1][:100],
            time_ago=time_ago,
            now=now,
            trackers=[(t.id, t.title) for t in trackers],
        )


class TrackerSessionApiController(ApiController):
    def __init__(self, *args, **kwargs):
        super().__init__(TrackerSession, model_enabled_field="trackers_enabled")

    def process_batch_create_args(self):
        form_data = request.form or request.json
        tracker_id = form_data.get('tracker_id')
        tracker = Tracker.get_by(id=tracker_id)
        if not tracker:
            raise Exception('No tracker with id {}'.format(tracker_id))
        api_key = form_data.get('api_key')
        if not tracker.api_key == api_key:
            raise Exception('Wrong API key for tracker with id {}'.format(tracker_id))
        login_user(tracker.user)
        sessions = form_data.get('sessions', [])
        return sessions, {'tracker_id': tracker_id, 'tracker': tracker}

    def process_batch_create_arg(self, batch_instance, common_params):
        title = batch_instance.get('title', '')
        title = title.strip()
        if not title:
            raise Exception("Empty tracker session title.")
        rv = dict(
            title=title,
        )
        started_ts = parse_ts(batch_instance['started_ts'])
        rv['started_ts'] = started_ts
        finished_ts = parse_ts(batch_instance['finished_ts'])
        if started_ts > finished_ts:
            raise Exception("End time can't be before start time.")
        rv['finished_ts'] = finished_ts
        rv['tracker_id'] = common_params.get('tracker_id', 0)
        batch_instance.pop('title')
        batch_instance.pop('started_ts')
        batch_instance.pop('finished_ts')
        # process metric_keys
        tracker = common_params.get('tracker')
        for i in range(1, 4):
            metric_k = getattr(tracker, "metric_{}_k".format(i), "")
            if metric_k:
                rv['metric_{}_v'.format(i)] = batch_instance.pop(metric_k, "")
        rv['meta'] = json.dumps(batch_instance)
        return rv


class MetricViewController(ViewController):
    def __init__(self, *args, **kwargs):
        super().__init__(Metric, model_enabled_field="trackers_enabled")

    def process_create_args(self):
        title = request.form.get('title', '')
        title = title.strip()
        if not title:
            raise Exception("Empty title of project.")
        rv = dict(
            title=title,
            filter_by=request.form.get('filter_by', ""),
            aggregate_by=request.form.get('aggregate_by', ""),
            aggregate_func=request.form.get('aggregate_func', ""),
        )
        return rv

    def process_metrics(self, metrics, ctx):
        rv = []
        for metric in metrics:
            current = dict(
                id=metric.id,
                title=metric.title,
                filter_by=metric.filter_by,
                aggregate_func=metric.aggregate_func,
                aggregate_by=metric.aggregate_by,
            )
            aggregate_value = 0
            filter_by = metric.filter_by.strip().lower()
            filter_ors = filter_by.split('or')
            metric_type, metric_n_v = "", ""

            for filter_or in filter_ors:
                filter_or_splitted = filter_or.split('=')
                filter_k = filter_or_splitted[0].strip()
                filter_v = filter_or_splitted[1].strip()

                query_filters = [
                    Tracker.thing_k == filter_k,
                    Tracker.user == current_user
                ]

                if filter_v != "*":
                    query_filters += [TrackerSession.title == filter_v]

                if ctx.beginning_of_date and ctx.end_of_date:
                    query_filters += [TrackerSession.get_filter_in_bounds(ctx.beginning_of_date, ctx.end_of_date)]

                tracker_sessions = TrackerSession.query.join(Tracker).filter(*query_filters).all()
                if tracker_sessions:
                    tracker_session = tracker_sessions[0]
                    for i in range(1, 4):
                        if metric.aggregate_by.code == getattr(Metric, "AGGR_BY_METRIC_{}".format(i)):
                            metric_n_v = "metric_{}_v".format(i)
                            metric_type = getattr(tracker_session.tracker, "metric_{}_t".format(i))
                            break
                else:
                    continue
                for tracker_session in tracker_sessions:
                    if metric.aggregate_by.code == Metric.AGGR_BY_TIME:
                        metric_type = Tracker.METRIC_T_INT_SEC
                        aggregate_value += (tracker_session.finished_ts - tracker_session.started_ts).seconds
                    elif metric_type in [Tracker.METRIC_T_INT_SEC, Tracker.METRIC_T_INT]:
                        aggregate_value += int(getattr(tracker_session, metric_n_v))
                    else:
                        aggregate_value += getattr(tracker_session, metric_n_v)
            current['aggregate'] = aggregate_value
            if metric_type == Tracker.METRIC_T_INT_SEC:
                current['aggregate_format'] = format_seconds_to_human(aggregate_value)
            else:
                current['aggregate_format'] = aggregate_value
            rv.append(current)
        return rv

    def get_template_ctx(self, instances, ctx):
        return dict(
            aggregate_funcs=Metric.AGGR_FUNCS,
            aggregate_bys=Metric.AGGR_BYS,
            metrics=self.process_metrics(instances, ctx),
        )
