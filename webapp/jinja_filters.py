import re

from flask_security import current_user
from inflector import English
from markupsafe import escape

from webapp.utils import to_user_tz as u_to_user_tz, format_seconds_to_human


def register_filters(app):
    @app.template_filter('format_dt')
    def format_datetime(dt, formatting="%A, %d %b %Y"):
        return dt.strftime(formatting)

    @app.template_filter('to_user_tz')
    def to_user_tz(dt):
        return u_to_user_tz(dt, current_user)

    @app.template_filter('format_sec_to_human')
    def format_sec_to_human(s):
        return format_seconds_to_human(s)

    @app.template_filter('format_timedelta')
    def format_timedelta(td):
        return format_seconds_to_human(td.seconds)

    @app.template_filter('pluralize')
    def pluralize_(word):
        return English().pluralize(word)

    @app.template_filter('hl_search')
    def highlight(text, query, n_hl=3):
        snippets = []
        for word in query.split():
            match_positions = [m.start() for m in re.finditer(word, text)]
            for mpos in match_positions:
                match_before = ' '.join(text[:mpos].split()[-3:])
                match = text[mpos:mpos + len(word)]
                match_after = ' '.join(text[mpos:].split()[1:4])
                snippet = "%s <b>%s</b> %s" % (escape(match_before), escape(match),
                                               escape(match_after))
                snippets.append(snippet)
        return '...'.join(snippets[:n_hl])
