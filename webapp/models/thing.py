from collections import defaultdict
from pprint import pprint
from passlib.apps import custom_app_context as pwd_context

from datetime import datetime
from pprint import pprint

from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import text

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///thing.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
db = SQLAlchemy(app)


class ThingDef(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    name = db.Column(db.Unicode)
    prop = db.Column(db.Unicode)
    key = db.Column(db.Unicode)
    val = db.Column(db.Unicode)


class Thing(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    name = db.Column(db.Unicode)

    created_dt = db.Column(db.Integer)


class ThingData(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    thing_id = db.Column(db.Integer, db.ForeignKey('thing.id'))
    thing = db.relationship("Thing")

    prop = db.Column(db.Unicode)
    val = db.Column(db.Unicode)


def create_things_def(things):
    for thing_name, thing in things.items():
        thing_def = thing.get(THINGS_PROPS, dict())
        for thing_prop, prop_kvs in thing_def.items():
            for prop_k, prop_v in prop_kvs.items():
                thing_def = ThingDef(name=thing_name,
                                     prop=thing_prop,
                                     key=prop_k,
                                     val=prop_v)
                db.session.add(thing_def)
    db.session.commit()


class ThingORM(object):
    def __init__(self, thing_name):
        self.thing_name = thing_name

    def create(self, **kwargs):
        resolved_props = dict()

        thing_props = [t.prop for t in ThingDef.query.filter_by(name=self.thing_name).all()]

        for declared_prop, declared_prop_v in kwargs.items():
            if declared_prop in thing_props:
                resolved_props[declared_prop] = declared_prop_v

        thing = Thing(name=self.thing_name,
                      created_dt=datetime.utcnow())
        for prop_k, prop_v in resolved_props.items():
            thing_data = ThingData(thing=thing,
                                   prop=prop_k,
                                   val=prop_v)
            db.session.add(thing_data)

        db.session.commit()
        return thing.id

    def _convert_thing_data(self, instance, thing_datas, thing_defs):
        rv = dict()
        rv["props"] = defaultdict(dict)
        if thing_defs and thing_datas:
            for thing_def in thing_defs:
                rv["props"][thing_def.prop][thing_def.key] = thing_def.val
            rv["data"] = {
                data.prop: data.val for data in thing_datas
            }
            rv["data"]["id"] = instance.id
        return rv

    def _get_one_by_id(self, instance_id):
        instance = Thing.query.filter_by(id=instance_id).first()
        if instance:
            thing_defs = ThingDef.query.filter_by(name=instance.name).all()
            thing_datas = ThingData.query.filter_by(thing=instance).all()
            return instance, thing_defs, thing_datas
        return None, None, None

    def retrieve_one(self, instance_id):
        instance, thing_defs, thing_datas = self._get_one_by_id(instance_id)
        return [self._convert_thing_data(instance, thing_datas, thing_defs)]

    def _parse_triplet_filter(self, def_prop_filter):
        if len(def_prop_filter) != 3:
            return '', {}
        left, op, right = def_prop_filter
        txt = 'thing_data.prop=:{} and thing_data.val{}:val'.format(left, op)
        kw_params = {left: right, 'val': left}
        return txt, kw_params

    def retrieve(self, *args):
        defined_prop_filters = args
        filters, txt, kw_params = [], '', {}
        for def_prop_filter in defined_prop_filters:
            txt, kw_params = self._parse_triplet_filter(def_prop_filter)
        things_datas = ThingData.query.filter(text(txt)).params(**kw_params).all()
        return things_datas

    def update(self, instance_id, **kwargs):
        instance, thing_defs, thing_datas = self._get_one_by_id(instance_id)
        if not instance:
            return
        for prop, val in kwargs.items():
            thing_def_props = filter(lambda x: x.prop == prop, thing_defs)
            thing_data = list(filter(lambda x: x.prop == prop, thing_datas))
            if not thing_def_props:
                continue
            # todo: validate and convert format of data
            if len(thing_data) == 1:
                thing_data = thing_data[0]
                thing_data.val = val
            elif len(thing_data) > 1:
                raise Exception("{}.{} has more than 1 value".format(instance.name, prop))
            else:
                thing_data = ThingData(thing_id=instance_id,
                                       prop=prop,
                                       val=val)
            db.session.add(thing_data)
        db.session.commit()


THINGS_PROPS = "props"

LABEL_NOTIFY = "notify"
PROP_NOTIFY = "notify_dt"

PROP_KEY_TYPE = "type"

TYPE_SHORT_TEXT = "short_text"
TYPE_PASSWORD = "password"
TYPE_LONG_TEXT = "long_text"
TYPE_BOOLEAN = "boolean"
TYPE_DATETIME = "datetime"

TYPE_DATETIME_RANGE = "datetime_range"
PROP_KEY_RANGE_THIS = "range_this"
PROP_KEY_RANGE_THAT = "range_that"
PROP_VAL_RANGE_START = "start"
PROP_VAL_RANGE_END = "end"

TYPE_RELATIONSHIP = "relationship"
PROP_KEY_REL_TYPE = "rel_type"
PROP_KEY_REL_TO = "rel_to"
PROP_VAL_REL_M_1 = "M:1"
PROP_VAL_REL_M_N = "M:N"

TYPE_THING = "thing"

things = {
    "user": {
        THINGS_PROPS: {
            "email": {
                PROP_KEY_TYPE: TYPE_SHORT_TEXT,
            },
            "password": {
                PROP_KEY_TYPE: TYPE_PASSWORD,
            }
        }
    },
    "reminder": {
        THINGS_PROPS: {
            "remind_about": {
                PROP_KEY_TYPE: TYPE_THING,
            },
            PROP_NOTIFY: {
                PROP_KEY_TYPE: TYPE_DATETIME
            },
        },
    },
    "project": {
        THINGS_PROPS: {
            "name": {
                PROP_KEY_TYPE: TYPE_SHORT_TEXT,
            },
        },
    },
    "task": {
        THINGS_PROPS: {
            "title": {
                PROP_KEY_TYPE: TYPE_SHORT_TEXT,
            },
            "body": {
                PROP_KEY_TYPE: TYPE_LONG_TEXT,
            },
            "is_done": {
                PROP_KEY_TYPE: TYPE_BOOLEAN,
            },
            "show_ts": {
                PROP_KEY_TYPE: TYPE_DATETIME_RANGE,
                PROP_KEY_RANGE_THIS: PROP_VAL_RANGE_START,
                PROP_KEY_RANGE_THAT: "due_ts",
            },
            "due_ts": {
                PROP_KEY_TYPE: TYPE_DATETIME_RANGE,
                PROP_KEY_RANGE_THIS: PROP_VAL_RANGE_END,
                PROP_KEY_RANGE_THAT: "show_ts",
            },
            "project": {
                PROP_KEY_TYPE: TYPE_RELATIONSHIP,
                PROP_KEY_REL_TYPE: PROP_VAL_REL_M_1,
                PROP_KEY_REL_TO: "project",
            },
            "user": {
                PROP_KEY_TYPE: TYPE_RELATIONSHIP,
                PROP_KEY_REL_TYPE: PROP_VAL_REL_M_1,
                PROP_KEY_REL_TO: "user",
            },
        },
    },
    "calendar": {
        THINGS_PROPS: {
            "name": {
                PROP_KEY_TYPE: TYPE_SHORT_TEXT,
            },
            "user": {
                PROP_KEY_TYPE: TYPE_RELATIONSHIP,
                PROP_KEY_REL_TYPE: PROP_VAL_REL_M_1,
                PROP_KEY_REL_TO: "user",
            },
        },
    },
    "event": {
        THINGS_PROPS: {
            "title": {
                PROP_KEY_TYPE: TYPE_SHORT_TEXT,
            },
            "body": {
                PROP_KEY_TYPE: TYPE_LONG_TEXT,
            },
            "start_ts": {
                PROP_KEY_TYPE: TYPE_DATETIME_RANGE,
                PROP_KEY_RANGE_THIS: PROP_VAL_RANGE_START,
                PROP_KEY_RANGE_THAT: "end_ts",
            },
            "end_ts": {
                PROP_KEY_TYPE: TYPE_DATETIME_RANGE,
                PROP_KEY_RANGE_THIS: PROP_VAL_RANGE_END,
                PROP_KEY_RANGE_THAT: "start_ts",
            },
            "calendar": {
                PROP_KEY_TYPE: TYPE_RELATIONSHIP,
                PROP_KEY_REL_TYPE: PROP_VAL_REL_M_1,
                PROP_KEY_REL_TO: "calendar",
            },
            "user": {
                PROP_KEY_TYPE: TYPE_RELATIONSHIP,
                PROP_KEY_REL_TYPE: PROP_VAL_REL_M_1,
                PROP_KEY_REL_TO: "user",
            },
        },
    },
    "inbox_entry": {
        THINGS_PROPS: {
            "body": {
                PROP_KEY_TYPE: TYPE_LONG_TEXT,
            },
            "is_processed": {
                PROP_KEY_TYPE: TYPE_BOOLEAN,
            },
            "user": {
                PROP_KEY_TYPE: TYPE_RELATIONSHIP,
                PROP_KEY_REL_TYPE: PROP_VAL_REL_M_1,
                PROP_KEY_REL_TO: "user",
            },
        },
    },
    "goal": {
        THINGS_PROPS: {
            "name": {
                PROP_KEY_TYPE: TYPE_SHORT_TEXT,
            },
            "user": {
                PROP_KEY_TYPE: TYPE_RELATIONSHIP,
                PROP_KEY_REL_TYPE: PROP_VAL_REL_M_1,
                PROP_KEY_REL_TO: "user",
            },
        },
    },
    "group_goal": {
        THINGS_PROPS: {
            "name": {
                PROP_KEY_TYPE: TYPE_SHORT_TEXT,
            },
            "admin": {
                PROP_KEY_TYPE: TYPE_RELATIONSHIP,
                PROP_KEY_REL_TYPE: PROP_VAL_REL_M_1,
                PROP_KEY_REL_TO: "user",
            },
            "participants": {
                PROP_KEY_TYPE: TYPE_RELATIONSHIP,
                PROP_KEY_REL_TYPE: PROP_VAL_REL_M_N,
                PROP_KEY_REL_TO: "user",
            },
        },
    }
}


def setup():
    app.app_context().push()
    db.create_all()
    create_things_def(things)


def encrypt_password(password):
    return pwd_context.hash(password)


def verify_password(password, hash):
    return pwd_context.verify(password, hash)


setup()
user_orm = ThingORM("user")
user_id = user_orm.create(email=u'admin@app.com', password=encrypt_password('password'))
user_orm.retrieve_one(instance_id=user_id)

task_orm = ThingORM("task")
task_id = task_orm.create(title="Do something", user=user_id)
tasks = task_orm.retrieve_one(instance_id=task_id)
pprint(tasks)

task_orm.update(instance_id=task_id,
                title="completed something",
                is_done="True")
tasks = task_orm.retrieve(('title', '=', 'ok'))
pprint(tasks)
