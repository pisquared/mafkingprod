from sqlalchemy_utils import ChoiceType

from webapp.model_controller import ModelController, Ownable, Archivable, Datable, Runnable
from webapp.store import db


class Tracker(db.Model, ModelController, Ownable, Datable):
    METRIC_T_INT = "INT"
    METRIC_T_STR = "STR"
    METRIC_T_INT_SEC = "INT_SEC"

    METRIC_TYPES = [
        (METRIC_T_INT, 'integer'),
        (METRIC_T_INT_SEC, 'integer_seconds'),
        (METRIC_T_STR, 'string'),
    ]

    title = db.Column(db.Unicode)
    api_key = db.Column(db.Unicode)

    thing_k = db.Column(db.Unicode)

    metric_1_k = db.Column(db.Unicode)
    metric_2_k = db.Column(db.Unicode)
    metric_3_k = db.Column(db.Unicode)

    metric_1_t = db.Column(ChoiceType(METRIC_TYPES), default=METRIC_T_INT)
    metric_2_t = db.Column(ChoiceType(METRIC_TYPES), default=METRIC_T_INT)
    metric_3_t = db.Column(ChoiceType(METRIC_TYPES), default=METRIC_T_INT)


class TrackerSession(db.Model, ModelController, Ownable, Datable, Runnable, Archivable):
    title = db.Column(db.Unicode)

    metric_1_v = db.Column(db.Integer)
    metric_2_v = db.Column(db.Integer)
    metric_3_v = db.Column(db.Integer)

    meta = db.Column(db.Unicode)

    tracker_id = db.Column(db.Integer, db.ForeignKey('tracker.id'))
    tracker = db.relationship("Tracker", backref="tracker_sessions")


class Metric(db.Model, ModelController, Ownable, Datable, Archivable):
    AGGR_FUNC_SUM = "SUM"
    AGGR_FUNC_AV = "AVERAGE"
    AGGR_FUNC_LV = "LAST_VALUE"

    AGGR_FUNCS = [
        (AGGR_FUNC_SUM, 'sum'),
        (AGGR_FUNC_AV, 'average'),
        (AGGR_FUNC_LV, 'last_value'),
    ]

    AGGR_BY_TIME = "TIME"
    AGGR_BY_METRIC_1 = "METRIC_1"
    AGGR_BY_METRIC_2 = "METRIC_2"
    AGGR_BY_METRIC_3 = "METRIC_3"

    AGGR_BYS = [
        (AGGR_BY_TIME, 'time'),
        (AGGR_BY_METRIC_1, 'metric_1'),
        (AGGR_BY_METRIC_2, 'metric_2'),
        (AGGR_BY_METRIC_3, 'metric_3'),
    ]

    title = db.Column(db.Unicode)
    filter_by = db.Column(db.Unicode)
    aggregate_func = db.Column(ChoiceType(AGGR_FUNCS), default=AGGR_FUNC_SUM)
    aggregate_by = db.Column(ChoiceType(AGGR_BYS), default=AGGR_BY_TIME)
