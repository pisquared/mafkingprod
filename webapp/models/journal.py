from webapp.model_controller import ModelController, Ownable, Archivable, Datable
from webapp.search import whooshee
from webapp.store import db


@whooshee.register_model('body')
class JournalEntry(db.Model, ModelController, Ownable, Datable, Archivable):
    body = db.Column(db.Unicode)
