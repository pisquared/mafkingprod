from webapp.model_controller import ModelController, Ownable, Archivable, Datable, Schedulable, Runnable
from webapp.search import whooshee
from webapp.store import db

tasks_projects = db.Table('tasks_projects',
                          db.Column('task_id', db.Integer, db.ForeignKey('task.id')),
                          db.Column('project_id', db.Integer, db.ForeignKey('project.id')))

tasks_contexts = db.Table('tasks_contexts',
                          db.Column('task_id', db.Integer, db.ForeignKey('task.id')),
                          db.Column('context_id', db.Integer, db.ForeignKey('context.id')))

tasks_labels = db.Table('tasks_labels',
                        db.Column('task_id', db.Integer, db.ForeignKey('task.id')),
                        db.Column('label_id', db.Integer, db.ForeignKey('label.id')))


@whooshee.register_model('title', 'body')
class Task(db.Model, ModelController, Ownable, Datable, Runnable, Schedulable, Archivable):
    title = db.Column(db.Unicode)
    body = db.Column(db.Unicode)

    priority = db.Column(db.Integer, default=0)
    is_done = db.Column(db.Boolean, default=False)

    projects = db.relationship("Project",
                               secondary=tasks_projects,
                               back_populates="tasks")

    contexts = db.relationship("Context",
                               secondary=tasks_contexts,
                               back_populates="tasks")

    labels = db.relationship("Label",
                             secondary=tasks_labels,
                             back_populates="tasks")


@whooshee.register_model('title', 'body')
class Project(db.Model, ModelController, Ownable, Datable, Archivable):
    title = db.Column(db.Unicode)
    body = db.Column(db.Unicode)

    tasks = db.relationship("Task",
                            secondary=tasks_projects,
                            back_populates="projects")


@whooshee.register_model('title', 'body')
class Context(db.Model, ModelController, Ownable, Datable, Archivable):
    title = db.Column(db.Unicode)
    body = db.Column(db.Unicode)

    tasks = db.relationship("Task",
                            secondary=tasks_contexts,
                            back_populates="contexts")


@whooshee.register_model('key', 'value')
class Label(db.Model, ModelController, Ownable, Datable, Archivable):
    key = db.Column(db.Unicode)
    value = db.Column(db.Unicode)

    tasks = db.relationship("Task",
                            secondary=tasks_labels,
                            back_populates="labels")
