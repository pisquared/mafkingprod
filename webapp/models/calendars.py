from webapp.model_controller import ModelController, Ownable, Archivable, Datable, Recurrable
from webapp.search import whooshee
from webapp.store import db


@whooshee.register_model('title', 'body')
class CalendarEvent(db.Model, ModelController, Ownable, Datable, Recurrable, Archivable):
    title = db.Column(db.Unicode)
    body = db.Column(db.Unicode)

    calendar_id = db.Column(db.Integer, db.ForeignKey('calendar.id'))
    calendar = db.relationship("Calendar", backref="events")


@whooshee.register_model('title', 'body')
class Calendar(db.Model, ModelController, Ownable, Datable, Archivable):
    title = db.Column(db.Unicode)
    body = db.Column(db.Unicode)
