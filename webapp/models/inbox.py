from webapp.model_controller import ModelController, Ownable, Archivable, Datable
from webapp.search import whooshee
from webapp.store import db


@whooshee.register_model('body')
class InboxEntry(db.Model, ModelController, Ownable, Datable, Archivable):
    body = db.Column(db.Unicode)

    is_processed = db.Column(db.Boolean, default=False)
    processed_type = db.Column(db.String)
    processed_instance_id = db.Column(db.Integer)
