from webapp.model_controller import ModelController, Ownable, Archivable, Datable
from webapp.search import whooshee
from webapp.store import db


@whooshee.register_model('body')
class Note(db.Model, ModelController, Ownable, Datable, Archivable):
    body = db.Column(db.Unicode)
