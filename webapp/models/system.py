from sqlalchemy_utils import ChoiceType

from webapp.model_controller import ModelController, Ownable, Datable
from webapp.store import db


class Settings(db.Model, ModelController, Ownable):
    inbox_entries_enabled = db.Column(db.Boolean, default=True)

    tasks_enabled = db.Column(db.Boolean, default=True)
    tasks_projects_enabled = db.Column(db.Boolean, default=False)
    tasks_contexts_enabled = db.Column(db.Boolean, default=False)
    tasks_labels_enabled = db.Column(db.Boolean, default=False)
    tasks_due_ts_enabled = db.Column(db.Boolean, default=False)
    tasks_start_ts_enabled = db.Column(db.Boolean, default=False)

    calendar_events_enabled = db.Column(db.Boolean, default=False)
    calendars_enabled = db.Column(db.Boolean, default=False)

    trackers_enabled = db.Column(db.Boolean, default=False)
    notes_enabled = db.Column(db.Boolean, default=False)
    journal_enabled = db.Column(db.Boolean, default=False)

    def get_bool_fields(self):
        return [x for x in dir(self) if type(getattr(self, x)) is bool]


class Message(db.Model, ModelController, Ownable):
    TYPE_INFO = "INFO"
    TYPE_WARNING = "WARNING"
    TYPE_ERROR = "ERROR"

    TYPES = [
        (TYPE_INFO, 'info'),
        (TYPE_WARNING, 'warning'),
        (TYPE_ERROR, 'error'),
    ]

    body = db.Column(db.Unicode)
    type = db.Column(ChoiceType(TYPES), default=TYPE_INFO)

    url_context = db.Column(db.String, default='/')

    dismissed = db.Column(db.Boolean, default=False)
    dismissed_ts = db.Column(db.DateTime)


class Log(db.Model, ModelController, Ownable, Datable):
    body = db.Column(db.Unicode)
