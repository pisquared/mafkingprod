# Mafkingprod / Prod.txt

Expand on the idea of [todo.txt](https://github.com/todotxt/todo.txt) for data and [motherfuckingwebsite](http://motherfuckingwebsite.com) for design and include:
* tasks / todos - binary done entity with optional `start` time, `due` time, `+projects`, `@contexts`, `key:value` labels
* events - continuous entity with optional `@calendar`
* reminders - re-buzzing entity until `dismissed:<datetime>` label
* activities - past/present complete entities with optional `key:value` labels
* goals - future "to complete" entity. Toghether with `activities` the interface can provide success reports.
* trackers - expected to be more machine readable, client trackers dump various activities happening around the user
* notes - freeform markdown entities with optional `key:value` labels
* journal entries - freeform date-timed entities with optional `@journal`
* inbox entries (as per [GTD inbox](https://gettingthingsdone.com/)) - catch-all entity for future processing

The user can choose which entities are enabled in settings interface. By default, enable `inbox` and `tasks`.

The user should be able to create / search / filter / edit / archive / delete entries within the web interface which should reflect in the corresponding `txt` files.

All entities have a corresponsing `<entity>.txt` file with specific format described below. 

Archived entries are stored inside `<entity>_archive.txt` files. 

These `txt` files can be imported/exported at will which will sync the underlying database of the webapp. The database and search index are for optimization purposes.

Optionally the txt files are also synced with user's [Dropbox](https://www.dropbox.com/) account.

`inbox.txt` is a bit more special entity - this is a catch-all place to dump anything which isn't decided yet what entity it should be. An entry can be prefixed with `<entity>:` (e.g. `todo:`) and the rest of the entry will be processed according to the rules of the `entity`. Alternatively, an inbox entry can be later transformed into one of the others through the web interface.

All canonical datetimes are in [Combined datetime ISO-8061](https://en.wikipedia.org/wiki/ISO_8601#Combined_date_and_time_representations) format, i.e. `2007-04-05T12:30:15-02:00`. 

All repeating things are in [crontab](https://en.wikipedia.org/wiki/Cron)-like entries.

Labels are in `key:value` if single word and `"long key:long value"` if whitespace occurs in either.

## todo.txt

The original [todo.txt format](https://github.com/todotxt/todo.txt) is:

```
x (A) 2016-05-20 2016-04-30 do something +project @context key:value
c pri completion creation   text .......
```

We expand it a bit keeping it backward compatible. The dates are within square brackets, separate with comma start and end times. Parse the text part with the dates
```
x (A) 2016-05-20 2016-04-30 [start_ts, due_ts] do something +project/subproject @context key:value #tag << EOF
body of the task
EOF
c pri completion creation   text .......
```

* add [(start_ts,) (due_ts)]. If only one date is given, assume due_ts. If there is a [,] assume start_ts. Human parse these dates with a best effort.
* add subprojects with optional /
* add multiline support with << EOF


## events.txt

Use `@calendar` to specify a calendar, otherwise it goes to default. 

The user can try to add anything inside of `[]` and we should try best effort human parsing.

```
[weekdays from 9am to 5pm] work @calendar ---> [0 9 * * *, 0 17 * * *] work @calendar
[every day 10pm to 6am] sleep ---> [0 22 * * *, 0 6 * * *] sleep
[today at 5pm] meet sarah ---> [2018-07-08T17:00:00, 2018-07-08T18:00:00] meet sarah
[every year on 27 jan] my birthday @birthdays --> [0 0 27 1 *, 23 59 27 1 *] my birthday @birthdays
```

## reminders.txt

Same as events but with just a single point in time. Reminders should also keep buzzing through whatever notification interface until dismissed, signified by a kv-pair `dismissed:<time>`.

```
[monthly on 15th] pay the rent dismissed:2018-03-15 ---> [0 12 15 * *] dismissed:2018-03-15 pay the rent dismissed:2018-03-15T12:02:00
```

## activities.txt

This is actually activity sessions - [start/end time] and activity name. Additional `key:value` labels for info such as `distance:5km` or whatever are allowed.

```
[today at 5 for an hour] running ---> [2018-07-08T17:00:00, 2018-07-08T18:00:00] running
```

## goals.txt

The format of timing is:
* `MIN|MAX` - at least/at most
* `Xx DAY|WEEK|FORTHNIGHT|MONTH|YEAR` - X times a day/week/forthnight/month/year
* `Y m|h` - for Y minutes/hours

And/or:
* `YYYY-MM-DD` - deadline for this goal

And then add an activity.

```
[2018-06-15] study for interview
[at least 5 times a week for 20 min] running   ---> [MIN 5x WEEK 20m] running
```

## trackers.txt

This is actually tracker session. The trackers are expected to be a bit more-machine readable.

* `@host` - machine hostname of the client
* `+tracker_type` - tracker type

The rest is obvious.

```
[2018-06-15T18:00:00, 2018-06-15T18:00:30] Home application tracker +desktop_app_tracker @host "chrome:15s" "terminal:12s"
[2018-06-15T18:00:00, 2018-06-15T18:00:30] Home Domain tracker +domain_tracker @host "messenger.com:15s" "reddit.com:12s"
```

## notes.txt

Notes have title and valid markdown until next title.

```
# python cheat sheet "label:python stuff" color:blue
## subheading
MARKDOWN someting something dark side 
something else

# list of movies
* analyze this
* analyze that
* other
```

## journal.txt
Journal entries. Same as notes but the title is the creation date in some format of this journal entry.

```
# 2018-03-18
MARKDOWN something something dark side

# 2018-03-18T18:00:00
MARKDOWN something something dark side
```

## inbox.txt
Each entry can start with `<entity>:` and format of one of the previous entries. If it doesn't, the inbox entry is unprocessed and has a format of journal above. When an inbox entry is processed, it is also archived in the default `inbox_archive.txt`

```
# 2018-03-18T18:00:00
I don't know yet what this is but needs to be put somewhere.
```

