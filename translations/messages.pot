# Translations template for PROJECT.
# Copyright (C) 2018 ORGANIZATION
# This file is distributed under the same license as the PROJECT project.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2018.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2018-03-12 01:21+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.5.3\n"

#: webapp/strings.py:3
msgid ""
"\n"
"<p>This is your inbox. Put anything useless here. Free your goddamn "
"mind.</p>\n"
"<p>If you want to be smart-ass about it and create a todo, prefix the "
"thing with \"todo:\".</p>\n"
"<p>Example:</p>\n"
"<blockquote>\n"
"    todo: kill Bill\n"
"</blockquote>\n"
msgstr ""

#: webapp/strings.py:12
msgid ""
"\n"
"<p>Tasks are things that you want to do.</p>\n"
"<blockquote>\n"
"    Do or do not. There is no try\n"
"    <footer>\n"
"        <cite>That small green motherfucker from that space movie</cite>\n"
"    </footer>\n"
"</blockquote>\n"
msgstr ""

#: webapp/templates/security/register_user.html:43
msgid "Register"
msgstr ""

