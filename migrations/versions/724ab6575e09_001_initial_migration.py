"""001 initial migration

Revision ID: 724ab6575e09
Revises: 
Create Date: 2018-03-18 14:47:54.513609

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '724ab6575e09'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('role',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('name', sa.String(length=80), nullable=True),
    sa.Column('description', sa.String(length=255), nullable=True),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('name')
    )
    op.create_table('user',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('email', sa.String(length=255), nullable=True),
    sa.Column('password', sa.String(length=255), nullable=True),
    sa.Column('timezone', sa.String(), nullable=True),
    sa.Column('tz_offset_seconds', sa.Integer(), nullable=True),
    sa.Column('active', sa.Boolean(), nullable=True),
    sa.Column('confirmed_at', sa.DateTime(), nullable=True),
    sa.PrimaryKeyConstraint('id'),
    sa.UniqueConstraint('email')
    )
    op.create_table('calendar',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('archived_ts', sa.DateTime(), nullable=True),
    sa.Column('archived', sa.Boolean(), nullable=True),
    sa.Column('created_ts', sa.DateTime(), nullable=True),
    sa.Column('modified_ts', sa.DateTime(), nullable=True),
    sa.Column('title', sa.Unicode(), nullable=True),
    sa.Column('body', sa.Unicode(), nullable=True),
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['user_id'], ['user.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('context',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('archived_ts', sa.DateTime(), nullable=True),
    sa.Column('archived', sa.Boolean(), nullable=True),
    sa.Column('created_ts', sa.DateTime(), nullable=True),
    sa.Column('modified_ts', sa.DateTime(), nullable=True),
    sa.Column('title', sa.Unicode(), nullable=True),
    sa.Column('body', sa.Unicode(), nullable=True),
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['user_id'], ['user.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('inbox_entry',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('archived_ts', sa.DateTime(), nullable=True),
    sa.Column('archived', sa.Boolean(), nullable=True),
    sa.Column('created_ts', sa.DateTime(), nullable=True),
    sa.Column('modified_ts', sa.DateTime(), nullable=True),
    sa.Column('body', sa.Unicode(), nullable=True),
    sa.Column('is_processed', sa.Boolean(), nullable=True),
    sa.Column('processed_type', sa.String(), nullable=True),
    sa.Column('processed_instance_id', sa.Integer(), nullable=True),
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['user_id'], ['user.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('label',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('archived_ts', sa.DateTime(), nullable=True),
    sa.Column('archived', sa.Boolean(), nullable=True),
    sa.Column('created_ts', sa.DateTime(), nullable=True),
    sa.Column('modified_ts', sa.DateTime(), nullable=True),
    sa.Column('key', sa.Unicode(), nullable=True),
    sa.Column('value', sa.Unicode(), nullable=True),
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['user_id'], ['user.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('log',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('created_ts', sa.DateTime(), nullable=True),
    sa.Column('modified_ts', sa.DateTime(), nullable=True),
    sa.Column('body', sa.Unicode(), nullable=True),
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['user_id'], ['user.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('message',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('body', sa.Unicode(), nullable=True),
    sa.Column('type', sa.String(length=255), nullable=True),
    sa.Column('url_context', sa.String(), nullable=True),
    sa.Column('dismissed', sa.Boolean(), nullable=True),
    sa.Column('dismissed_ts', sa.DateTime(), nullable=True),
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['user_id'], ['user.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('project',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('archived_ts', sa.DateTime(), nullable=True),
    sa.Column('archived', sa.Boolean(), nullable=True),
    sa.Column('created_ts', sa.DateTime(), nullable=True),
    sa.Column('modified_ts', sa.DateTime(), nullable=True),
    sa.Column('title', sa.Unicode(), nullable=True),
    sa.Column('body', sa.Unicode(), nullable=True),
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['user_id'], ['user.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('roles_users',
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.Column('role_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['role_id'], ['role.id'], ),
    sa.ForeignKeyConstraint(['user_id'], ['user.id'], )
    )
    op.create_table('settings',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('inbox_entries_enabled', sa.Boolean(), nullable=True),
    sa.Column('tasks_enabled', sa.Boolean(), nullable=True),
    sa.Column('tasks_projects_enabled', sa.Boolean(), nullable=True),
    sa.Column('tasks_contexts_enabled', sa.Boolean(), nullable=True),
    sa.Column('tasks_labels_enabled', sa.Boolean(), nullable=True),
    sa.Column('tasks_due_ts_enabled', sa.Boolean(), nullable=True),
    sa.Column('tasks_start_ts_enabled', sa.Boolean(), nullable=True),
    sa.Column('calendar_events_enabled', sa.Boolean(), nullable=True),
    sa.Column('calendars_enabled', sa.Boolean(), nullable=True),
    sa.Column('trackers_enabled', sa.Boolean(), nullable=True),
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['user_id'], ['user.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('task',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('archived_ts', sa.DateTime(), nullable=True),
    sa.Column('archived', sa.Boolean(), nullable=True),
    sa.Column('created_ts', sa.DateTime(), nullable=True),
    sa.Column('modified_ts', sa.DateTime(), nullable=True),
    sa.Column('to_start_ts', sa.DateTime(), nullable=True),
    sa.Column('to_finish_ts', sa.DateTime(), nullable=True),
    sa.Column('is_done', sa.Boolean(), nullable=True),
    sa.Column('all_day', sa.Boolean(), nullable=True),
    sa.Column('started_ts', sa.DateTime(), nullable=True),
    sa.Column('finished_ts', sa.DateTime(), nullable=True),
    sa.Column('title', sa.Unicode(), nullable=True),
    sa.Column('body', sa.Unicode(), nullable=True),
    sa.Column('priority', sa.Integer(), nullable=True),
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['user_id'], ['user.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('calendar_event',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('archived_ts', sa.DateTime(), nullable=True),
    sa.Column('archived', sa.Boolean(), nullable=True),
    sa.Column('created_ts', sa.DateTime(), nullable=True),
    sa.Column('modified_ts', sa.DateTime(), nullable=True),
    sa.Column('to_start_ts', sa.DateTime(), nullable=True),
    sa.Column('to_finish_ts', sa.DateTime(), nullable=True),
    sa.Column('all_day', sa.Boolean(), nullable=True),
    sa.Column('recurring_to_start_ts', sa.DateTime(), nullable=True),
    sa.Column('recurring_to_finish_ts', sa.DateTime(), nullable=True),
    sa.Column('recurring_type', sa.String(length=255), nullable=True),
    sa.Column('recurring_every', sa.Integer(), nullable=True),
    sa.Column('recurring_weekly_mask', sa.String(), nullable=True),
    sa.Column('recurring_ends_type', sa.String(length=255), nullable=True),
    sa.Column('recurring_ends_after_x', sa.Integer(), nullable=True),
    sa.Column('recurring_ends_on', sa.DateTime(), nullable=True),
    sa.Column('recurring_ended_on', sa.DateTime(), nullable=True),
    sa.Column('recurring_off_on', sa.String(), nullable=True),
    sa.Column('title', sa.Unicode(), nullable=True),
    sa.Column('body', sa.Unicode(), nullable=True),
    sa.Column('calendar_id', sa.Integer(), nullable=True),
    sa.Column('user_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['calendar_id'], ['project.id'], ),
    sa.ForeignKeyConstraint(['user_id'], ['user.id'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('tasks_contexts',
    sa.Column('task_id', sa.Integer(), nullable=True),
    sa.Column('context_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['context_id'], ['context.id'], ),
    sa.ForeignKeyConstraint(['task_id'], ['task.id'], )
    )
    op.create_table('tasks_labels',
    sa.Column('task_id', sa.Integer(), nullable=True),
    sa.Column('label_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['label_id'], ['label.id'], ),
    sa.ForeignKeyConstraint(['task_id'], ['task.id'], )
    )
    op.create_table('tasks_projects',
    sa.Column('task_id', sa.Integer(), nullable=True),
    sa.Column('project_id', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['project_id'], ['project.id'], ),
    sa.ForeignKeyConstraint(['task_id'], ['task.id'], )
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('tasks_projects')
    op.drop_table('tasks_labels')
    op.drop_table('tasks_contexts')
    op.drop_table('calendar_event')
    op.drop_table('task')
    op.drop_table('settings')
    op.drop_table('roles_users')
    op.drop_table('project')
    op.drop_table('message')
    op.drop_table('log')
    op.drop_table('label')
    op.drop_table('inbox_entry')
    op.drop_table('context')
    op.drop_table('calendar')
    op.drop_table('user')
    op.drop_table('role')
    # ### end Alembic commands ###
