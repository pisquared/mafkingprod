#!/usr/bin/env bash
set -e

IP=$1
if [ -z $IP ]; then
    echo "Provide IP as first argument"
    exit 1;
fi

PROJECT=mafkingprod
DOMAIN="${PROJECT}.com"
PROJECT_DIR="/var/www/${DOMAIN}"

USER="${PROJECT}_user"

echo "LOCAL: Connecting to root@$IP - 1. root provision #1"
echo "======================================="
ssh root@$IP "bash -s" < provision/prod_1_root.sh

echo "LOCAL: Copy SSH key to root@$IP:/home/$USER/.ssh"
echo "======================================="
scp sensitive.py root@$IP:/home/$USER/
scp ~/.ssh/pi2_key_external root@$IP:/home/$USER/.ssh/
scp ~/.ssh/pi2_key_external.pub root@$IP:/home/$USER/.ssh/authorized_keys

echo "LOCAL: Connecting to root@$IP - 2. root provision"
echo "======================================="
ssh root@$IP "bash -s" < provision/prod_2_root.sh

echo "LOCAL: Connecting to $USER@$IP - 3. user provision"
echo "======================================="
ssh $USER@$IP "bash -s" < provision/prod_3_user.sh

echo "LOCAL: Connecting to root@$IP - 4. root provision"
echo "======================================="
ssh root@$IP "bash -s" < provision/prod_4_root.sh