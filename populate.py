import time

from flask_security import SQLAlchemyUserDatastore
from flask_security.utils import hash_password

import sensitive
from webapp.models.auth import Role, User
from webapp.store import db
from webapp import app

db.init_app(app)

user_datastore = SQLAlchemyUserDatastore(db, User, Role)

app.app_context().push()
db.create_all()

# TODO: Handle DST
user = user_datastore.create_user(email=u'admin@app.com',
                                  password=hash_password(sensitive.ADMIN_PASSWORD),
                                  timezone=time.tzname[0],
                                  tz_offset_seconds=-time.timezone)
db.session.commit()
